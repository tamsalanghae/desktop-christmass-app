const {app, BrowserWindow} = require('electron')

function createWindow() {
    const app = new BrowserWindow({
        width: 800,
        height: 400,
        backgroundColor: 'white',
        webPreferences: {
            nodeIntegration: false,
            worldSafeExecuteJavaScript: true,
            contextIsolation: true
        }
    })

    app.loadFile('index.html');
}

app.whenReady().then(createWindow)